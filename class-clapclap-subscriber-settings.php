<?php
/**
 * Plugin Name: ClapClap Subscriber Settings
 */
class ClapClap_Subscriber_Settings {
	public function init() {
		load_plugin_textdomain( 'clapclap-subscriber-settings', false, dirname( plugin_basename( __FILE__ ) ) );
		add_action( 'parse_request', array( __CLASS__, 'update_settings' ) );
		add_action(
			'woocommerce_edit_account_form_end',
			function () {
				echo wp_kses_post( self::get_subscriber_settings() );
			}
		);
	}

	public static function update_settings(): void {
		global $wpdb;

		$nonce                            = empty( $_REQUEST['_wpnonce'] ) ? '' : sanitize_key( $_REQUEST['_wpnonce'] );
		$show_subscriber_data_to_creators = '';
		if ( isset( $_GET['show_subscriber_data_to_creators'] ) && wp_verify_nonce( $nonce, 'show_subscriber_data_to_creators_nonce' ) ) {
			$show_subscriber_data_to_creators = intval( $_GET['show_subscriber_data_to_creators'] );
		}

		if ( ! is_int( $show_subscriber_data_to_creators ) ) {
			return;
		}

		$user_id = get_current_user_id();
		if ( ! $user_id ) {
			// translators: %1$s and %2$s are link tags.
			wc_add_notice( wc_kses_notice( sprintf( __( 'You need an active subscription to change your settings. %1$sSubscribe now%2$s', 'clapclap-subscriber-settings' ), '<a href="' . get_permalink( 38 ) . '" class="button">', '</a>' ) ), 'notice' );
			return;
		}

		if ( is_int( $show_subscriber_data_to_creators ) ) {
			update_user_option( $user_id, 'show_subscriber_data_to_creators', $show_subscriber_data_to_creators );

			// translators: %s name of the creator.
			wc_add_notice( wc_kses_notice( __( 'You updated your settings.', 'clapclap-subscriber-settings' ) ), 'success' );
		}
	}

	public static function get_subscriber_settings(): void {
		$user_id             = get_current_user_id();
		$data_already_shared = get_user_option( 'show_subscriber_data_to_creators', $user_id );
		if ( $data_already_shared ) {
			$button_url  = '?show_subscriber_data_to_creators=0';
			$button_text = __( 'Hide my data from creators', 'clapclap-subscriber-settings' );

		} else {
			$button_url  = '?show_subscriber_data_to_creators=1';
			$button_text = __( 'Show my data to creators', 'clapclap-subscriber-settings' );
		}

		echo '<h2 id="share-daya-with-creators">' . esc_html__( 'Share data with creators', 'clapclap-subscriber-settings' ) . '</h2>';
		if ( ! $data_already_shared ) {
			echo '<p>' . esc_html__( 'You are currently not sharing your data with creators.', 'clapclap-subscriber-settings' ) . ' <a href="' . esc_url( get_permalink( 2136 ) ) . '">' . esc_html__( 'Read more.', 'clapclap-subscriber-settings' ) . '</a></p>';
		}
		echo '<div class="follow-button-wrapper"><a href="' . esc_url( wp_nonce_url( $button_url, 'show_subscriber_data_to_creators_nonce' ) ) . '" class="follow-button button">' . esc_html( $button_text ) . '</a></div>';
	}
}

$clapclap_subscriber_settings = new ClapClap_Subscriber_Settings();
add_action( 'init', array( $clapclap_subscriber_settings, 'init' ) );
